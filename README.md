# Cao-Math

![Run tests](https://github.com/caolo-game/cao-math/workflows/Run%20tests/badge.svg)

Simple linalg and [Hexagonal grid](https://www.redblobgames.com/grids/hexagons/#hex-to-pixel) implementations to help the Cao-Lo project.

## Dependencies

See [here](https://rustwasm.github.io/book/game-of-life/setup.html)

## Building

```
wasm-pack build
```
