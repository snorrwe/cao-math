//! This module provides wrappings to perform operations on vectors in bulk

mod vec2;
mod vec3;

pub use vec2::*;
pub use vec3::*;
